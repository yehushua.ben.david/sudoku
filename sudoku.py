import pygame as pg, sys ,json


pg.init()
G = {}
G["sz"] = 100
G["game"]= [ [0  for i in range(9)] for j in range(9)]
try:
    G["game"]=json.load(open("sudoku"))
except:
    pass
for p in  G["game"] :
    print(p)
print(pg.font.get_default_font())
G["ff"]= pg.font.Font(pg.font.get_default_font(),G["sz"])
screen = pg.display.set_mode((9 * G["sz"], 9 * G["sz"]))
G["nb"]=[ pg.transform.scale(G["ff"].render(" "+str(i)+" ", True, (0, 0, 0)), (G["sz"], G["sz"])) for i in range(0,10)]
G["nbb"]=[ pg.transform.scale(G["ff"].render(" "+str(i)+" ", True, (120, 120, 255)), (G["sz"], G["sz"])) for i in range(0,10)]
G["nbr"]=[ pg.transform.scale(G["ff"].render(" "+str(i)+" ", True, (255, 0, 0)), (G["sz"], G["sz"])) for i in range(0,10)]
G["nbg"]=[ pg.transform.scale(G["ff"].render(" "+str(i)+" ", True, (0, 200, 0)), (G["sz"]//3, G["sz"]//3)) for i in range(0,10)]
def possible(x,y,g):
    rst=[]
    for i in range(9):
        rst.append(g[y][i])
        rst.append(g[i][x])
    for iy in range(3):
        for ix in range(3):
            rst.append(g[iy+(y//3)*3][ix+(x//3)*3])
    return rst;

def drawboard(gg):
    ff = gg["ff"]
    screen.fill(((255, 255, 255)))
    col = 0, 0, 0
    for x in range(3):
        for y in range(3):
            pg.draw.rect(screen, col,
                 (
                     3*x * gg["sz"],
                     3*y * gg["sz"],
                     3*gg["sz"],
                     3*gg["sz"]
                 ), 5
                 )
    for x in range(9):
        for y in range(9):
            ww = 1
            if gg["game"][y][x]:
                txt = gg["nb"][gg["game"][y][x]]

                screen.blit(txt, (
                    x * gg["sz"],
                    y * gg["sz"],
                    gg["sz"],
                    gg["sz"]
                ))
            else:
                pp = possible(x,y,gg["game"])

                p=-1
                for pi in range(1,10):

                    p+=1
                    px=p%3
                    py=p//3
                    if pi in pp:
                        continue
                    screen.blit(gg["nbg"][pi],
                                (
                        x * gg["sz"] +px*(gg["sz"]//3) ,
                                y * gg["sz"]+py*(gg["sz"]//3),
                                gg["sz"]//3,
                                gg["sz"]//3))

            pg.draw.rect(screen, col,
                         (
                             x * gg["sz"],
                             y * gg["sz"],
                              gg["sz"],
                              gg["sz"]
                         ),ww
                         )
    pg.display.update()

drawboard(G)

while True:

    for e in pg.event.get():
        #print(e)

        if e.type == pg.MOUSEBUTTONDOWN:
            y = e.pos[1] // G["sz"]
            x = e.pos[0] // G["sz"]

            if e.button == 1:
                print(y, x,":", G["game"][y][x])
                G["game"][y][x]=G["game"][y][x]+1

            if e.button == 3:
                G["game"][y][x]-=1
            G["game"][y][x]%=10
            print(y,x,":",G["game"][y][x])
            drawboard(G)
        if e.type == pg.MOUSEBUTTONUP:
            drawboard(G)
        if e.type == pg.QUIT:
            json.dump(G["game"],open("sudoku","w"))
            sys.exit(0)

